# Not Apple App store


## What is this?

An F-Droid Repository for open-source iOS apps that Apple is not a fan of.

## Description
Based on work described at
- https://android.izzysoft.de/articles/named/fdroid-simple-binary-repo?lang=en
- https://f-droid.org/en/docs/Setup_an_F-Droid_App_Repo/
- https://f-droid.org/en/2023/09/03/reproducible-builds-signing-keys-and-binary-repos.html 

## Usage

Go here to browse apps: TODO

Follow Alt-Store procedure to install apps: TODO

## Support
Email support@guardianproject.info with questions

## Roadmap
1. Build F-Droid Repo
2. Publish Repo
3. Promote Repo

## Contributing
If you have an app you would like to add, email support@guardianproject.info 

### Working with this repository

This is an [F-Droid](https://f-droid.org) repository.  This project holds all
metadata files and plugin files required for creating/operating
https://guardianproject.gitlab.io/pearappstore-web.

Working with this repository requires a [special version of
fdroidserver]{https://gitlab.com/uniqx/fdroidserver/-/tree/pearstore}. (We're
working on upstreaming IPA support for fdroidserver though.)

Basic useage:

```
# clone project
git clone https://gitlab.com/guardianproject/pearappstore.git
cd pearappstore

# Check for new IPA files on Github releases. (This will write the tags/version
# numbers of new relases to lcoal metadata files)
tools/fdroid_gh_releases_checkupdates.py --all

# Download all binaries from Github releases to local repo folder.
tools/fdroid_gh_fetch.py --all

# download latest source_code, for app store listings, screenshots, etc.
tools/fdroid_checkout_latest.py --all

# now proceed with the normal fdroid server publishing procedure ...
fdroid update
fdroid deploy
```

## Authors and acknowledgment
@uniqx @n8fr8

## License
TBD

## Project status
Hello
