#! /usr/bin/env python3

import os
import sys
import shutil
import pathlib
import argparse
import tempfile
import fdroidlib
import urllib.parse
import urllib.request


fdroid_summary = "fetch binaries from github releases"


def process(metadata_path):
    for repo_dir in ['repo', 'archive']:
        pathlib.Path(repo_dir).mkdir(parents=True, exist_ok=True)

    app_meta = fdroidlib.FDroidAppMetadata(metadata_path).load()
    if "X-Github-Releases" in app_meta.metadata:
        gh_info = fdroidlib.GitHubReleasesInfo(app_meta).fetch()

        missing_versions = {}
        for build in app_meta.metadata["Builds"]:
            repo_path = pathlib.Path(
                f"repo/{app_meta.package_name}_{build['versionCode']}.ipa"
            )
            archive_path = pathlib.Path(
                f"archive/{app_meta.package_name}_{build['versionCode']}.ipa"
            )
            if not repo_path.exists():
                if not archive_path.exists():
                    missing_versions[build["versionCode"]] = repo_path

        if missing_versions:
            gh_info = fdroidlib.GitHubReleasesInfo(app_meta).fetch()
            for version_code, repo_path in missing_versions.items():
                download_url = gh_info.find_by_vercode(version_code)["url"]
                print(
                    "downloading",
                    download_url,
                    "->",
                    repo_path,
                    "...",
                    end="",
                    flush=True,
                )
                with tempfile.TemporaryDirectory() as tmpdir:
                    tmp_path = pathlib.Path(tmpdir) / "file"
                    urllib.request.urlretrieve(download_url, tmp_path)
                    # wait for download to finish sucessfully, only then move
                    # only then move the file to repo folder, to avoid ending
                    # up with half finished downloaded files in repo folder
                    # couldn't find any checksums in github releases api
                    shutil.move(tmp_path, repo_path)
                print(" DONE")


def main():
    p = argparse.ArgumentParser()
    p.add_argument(
        "--all",
        "-a",
        action="store_true",
        default=False,
        help="check all apps with `Repo:` metadata pointing to github",
    )
    p.add_argument("PACKAGE_NAME", nargs="*", default=[])
    args = p.parse_args()

    if len(args.PACKAGE_NAME) == 0 and not args.all:
        print("error: please specify either at least one PACKAGE_NAME or --all")
        print("       (more help: fdroid fetch_gh --help)")
        sys.exit(1)

    if not os.path.exists("./build"):
        os.mkdir("./build")

    for p in fdroidlib.getMetadataPaths(args.PACKAGE_NAME, read_all=args.all):
        process(p)


if __name__ == "__main__":
    main()
