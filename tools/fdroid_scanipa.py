#! /usr/bin/env python3

import re
import os
import magic
import biplist
import zipfile
import tempfile
import argparse
import urllib.request


fdroid_summary = "inspect apple app files and display key information"


def main():
    p = argparse.ArgumentParser()
    p.add_argument("FILE_OR_URL")
    args = p.parse_args()

    downloadAndExtractPackageName(args.FILE_OR_URL)


def downloadAndExtractPackageName(path_or_url):
    u = urllib.parse.urlparse(path_or_url)
    if u.scheme == "https":
        with tempfile.TemporaryDirectory() as tmpdir:
            tmpIpaPath = os.path.join(tmpdir, "test.ipa")
            urllib.request.urlretrieve(path_or_url, tmpIpaPath)
            probeAndExtractPackageName(tmpIpaPath)
    elif u.scheme == "":
        probeAndExtractPackageName(path_or_url)


def probeAndExtractPackageName(local_ipa_path):
    observed_mime = magic.Magic(mime=True).from_file(local_ipa_path)
    if observed_mime == "application/x-ios-app":
        extractPackageName(local_ipa_path)
    elif observed_mime == "application/zip":
        extractPackageName(local_ipa_path)
    else:
        print("error: supplied file is not an IPA (mime: {})".format(observed_mime))


def extractPackageName(ipa_path):
    with zipfile.ZipFile(ipa_path) as ipa_zip:
        for info in ipa_zip.infolist():
            if re.match("Payload/[^/]*.app/Info.plist", info.filename):
                with ipa_zip.open(info) as plist_file:
                    plist = biplist.readPlist(plist_file)
                    print("packageName:", plist["CFBundleIdentifier"])
                    # "versionCode:" version_string_to_int(plist["CFBundleShortVersionString"])
                    print("versionName:", plist["CFBundleShortVersionString"])
                    # print("usage:", {k: v for k, v in plist.items() if 'Usage' in k})


if __name__ == "__main__":
    main()
