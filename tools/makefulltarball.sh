#! /bin/bash

set -euo pipefail

rm -rf pearappstore
mkdir -p pearappstore/fdroid
cp -r repo pearappstore/fdroid/repo
cp -r archive pearappstore/fdroid/archive
cp -r ../fdroidwebdash/build/web/* pearappstore

tar -czvf pearappstore.tar.gz pearappstore
gpg --armor --default-key 05529A5A --detach-sign pearappstore.tar.gz
