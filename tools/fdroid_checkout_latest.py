#! /usr/bin/env python3

import sys
import argparse
import fdroidlib


fdroid_summary = "checkout the latest source code of all apps"


def main():
    p = argparse.ArgumentParser()
    p.add_argument(
        "--all",
        "-a",
        action="store_true",
        default=False,
        help="check all apps with `Repo:` metadata pointing to github",
    )
    p.add_argument("PACKAGE_NAME", nargs="*", default=[])
    args = p.parse_args()

    if len(args.PACKAGE_NAME) == 0 and not args.all:
        print("error: please specify either at least one PACKAGE_NAME or --all")
        print("       (more help: fdroid fetch_gh --help)")
        sys.exit(1)

    for p in fdroidlib.getMetadataPaths(args.PACKAGE_NAME, read_all=args.all):
        checkout_latest_app_source_code(p)


def checkout_latest_app_source_code(metadata_path):
    """does a shallow checkout of the source code, which includes the fastlane metadata"""
    app_meta = fdroidlib.FDroidAppMetadata(metadata_path).load()
    app_meta.checkout_latest()


if __name__ == "__main__":
    main()
