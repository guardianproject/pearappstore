#! /usr/bin/env python3

import os
import sys
import argparse
import fdroidlib


fdroid_summary = "check github releases for new releases"


def process_gh_releases_checkupdate(metadata_path):
    app_meta = fdroidlib.FDroidAppMetadata(metadata_path).load()
    if "X-Github-Releases" in app_meta.metadata:
        gh_info = fdroidlib.GitHubReleasesInfo(app_meta).fetch()
        new_releases = list(gh_info.find_new_releases())
        if new_releases:
            print(f"found {len(new_releases)} new relases of '{app_meta.package_name}'")
        app_meta.metadata["Builds"].extend(new_releases)
        app_meta.store()
        return True
    return False


def main():
    p = argparse.ArgumentParser()
    p.add_argument(
        "--all",
        "-a",
        action="store_true",
        default=False,
        help="check all apps with `Repo:` metadata pointing to github",
    )
    p.add_argument("PACKAGE_NAME", nargs="*", default=[])
    args = p.parse_args()

    if len(args.PACKAGE_NAME) == 0 and not args.all:
        print("error: please specify either at least one PACKAGE_NAME or --all")
        print("       (more help: fdroid fetch_gh --help)")
        sys.exit(1)

    if not os.path.exists("./build"):
        os.mkdir("./build")

    cnt = 0
    for metadata_path in fdroidlib.getMetadataPaths(
        args.PACKAGE_NAME, read_all=args.all
    ):
        checked = process_gh_releases_checkupdate(metadata_path)
        cnt += 1 if checked else 0
    print(f"checked {cnt} apps for new updates")


if __name__ == "__main__":
    main()
