import re
import json
import glob
import shutil
import pathlib
import functools
import subprocess
import ruamel.yaml
import urllib.parse
import urllib.request

GH_REPO_PATH = re.compile(r"/([^/]+)/([^/]+)")


def getMetadataPaths(package_names=[], metadata_dir="metadata", read_all=False):
    if read_all:
        return glob.glob(f"{metadata_dir}/*.yml")
    if len(package_names) > 0:
        return [f"{metadata_dir}/{n}.yml" for n in package_names]
    return []


def semver_to_version_code(major, minor, patch):
    """convert semantic versioning scheme digits to an android version code number"""
    major = int(major)
    minor = int(minor)
    patch = int(patch)
    return major * 10**12 + minor * 10**6 + patch


class GitHubReleasesInfo:
    def __init__(self, app_meta):
        self.app_meta = app_meta

        if "Repo" not in self.app_meta.metadata:
            raise Exception(f"No 'Repo' value in '{self.app_meta.package_name}'")

        self.version_code_regex_semver = None
        if type(app_meta.metadata.get("X-Github-Releases")) == dict:
            if (
                type(app_meta.metadata["X-Github-Releases"].get("TagParserSemver"))
                == str
            ):
                self.version_code_regex_semver = re.compile(
                    app_meta.metadata["X-Github-Releases"]["TagParserSemver"]
                )

        repo_url = urllib.parse.urlparse(self.app_meta.metadata.get("Repo", ""))
        if not repo_url.scheme == "https":
            raise Exception(
                f"can't get GitHub info for {self.app_meta.package_name}, 'Repo' not a https url"
            )
        if not repo_url.netloc == "github.com":
            raise Exception(
                f"can't get GitHub info for {self.app_meta.package_name}, 'Repo' doesn't contain 'github.com'"
            )
        m = GH_REPO_PATH.match(repo_url.path)
        if m:
            self.account_name = m.group(1)
            self.project_name = (
                m.group(2)[:-4] if m.group(2).endswith(".git") else m.group(2)
            )
        else:
            raise Exception(
                f"could not parse 'Repo' value of {self.package_name}, must point to github repository"
            )

    def fetch(self):
        url = "https://api.github.com/repos/{}/{}/releases".format(
            self.account_name, self.project_name
        )

        with urllib.request.urlopen(url) as f:
            self.releases = json.load(f)

        return self

    def find_new_releases(self):
        if "Builds" not in self.app_meta.metadata:
            self.app_meta.metadata["Builds"] = []
        known_version_codes = [
            x["versionCode"] for x in self.app_meta.metadata["Builds"]
        ]
        for ipa_asset in self._elegible_ipa_assets():
            if ipa_asset["versionCode"] not in known_version_codes:
                yield {
                    "versionName": ipa_asset["name"],
                    "versionCode": ipa_asset["versionCode"],
                    "commit": ipa_asset["tag_name"],
                }

    def find_by_vercode(self, versionCode):
        for ipa_asset in self._elegible_ipa_assets():
            if ipa_asset["versionCode"] == versionCode:
                return ipa_asset

    def _elegible_ipa_assets(self):
        for release in self.releases:
            for asset in release["assets"]:
                if asset["name"].lower().endswith(".ipa"):
                    if self.version_code_regex_semver:
                        m = self.version_code_regex_semver.match(release["tag_name"])
                        if m:
                            yield {
                                "name": release["name"],
                                "tag_name": release["tag_name"],
                                "url": asset["browser_download_url"],
                                # "versionName": f"{m.group('major')}.{m.group('minor')}.{m.group('patch')}",
                                "versionCode": semver_to_version_code(
                                    m.group("major"), m.group("minor"), m.group("patch")
                                ),
                            }


class FDroidAppMetadata:
    def __init__(self, metadata_path):
        self.package_name = pathlib.Path(metadata_path).name.rstrip(".yml")
        self.path = pathlib.Path(metadata_path)

    def load(self):
        if not self.path.exists():
            raise Exception(f"can't load '{self.path}' file doesn't exists")

        with open(self.path, "r", encoding="utf-8") as f:
            self.metadata = ruamel.yaml.safe_load(f)

        if not self.metadata:
            self.metadata = []

        return self

    def store(self):
        with open(self.path, "w", encoding="utf-8") as f:
            ruamel.yaml.round_trip_dump(self.metadata, f)

    def find_latest_build(self):
        def newer_build(a, b):
            if a.get("versionCode", 0) > b.get("versionCode", 0):
                return a
            else:
                return b

        builds = self.metadata.get("Builds")
        if builds:
            return functools.reduce(newer_build, builds)
        return {}

    def checkout_latest(self):
        """checks out the latest known (metadata build entry) version to ./build"""
        latest_git_ref = self.find_latest_build().get("commit")

        if latest_git_ref:
            b_dir = pathlib.Path(f"./build/{self.package_name}")
            b_dir.mkdir(parents=True, exist_ok=True)

            try:
                if not (b_dir / ".git").exists():
                    subprocess.check_output(
                        [shutil.which("git"), "init"],
                        cwd=b_dir,
                        stderr=subprocess.STDOUT,
                    )
                    subprocess.check_output(
                        [
                            shutil.which("git"),
                            "remote",
                            "add",
                            "-f",
                            "origin",
                            self.metadata["Repo"],
                        ],
                        cwd=b_dir,
                        stderr=subprocess.STDOUT,
                    )

                subprocess.check_output(
                    [
                        shutil.which("git"),
                        "fetch",
                        "--depth=1",
                        "origin",
                        "tag",
                        latest_git_ref,
                        "--no-tags",
                    ],
                    cwd=b_dir,
                    stderr=subprocess.STDOUT,
                )
                subprocess.check_output(
                    [shutil.which("git"), "checkout", latest_git_ref],
                    cwd=b_dir,
                    stderr=subprocess.STDOUT,
                )
            except subprocess.CalledProcessError as e:
                print(f"error: {e}")
                raise e
